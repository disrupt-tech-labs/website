---
title: "Our Work"
description: ""
slug: "work"
image: work.jpg
keywords: ""
categories: 
    - ""
    - ""
date: 2017-10-31T21:28:43-05:00
draft: false
---

# MyoTherapy
We believe in the future of wearable technology. So we are building a brighter tomorrow using wearables. How? With the help of bright entrepreneurs and medical professionals. Oh, and the determination that access to technology should be limitless. Enter MyoTherapy, a bold new way to use existing technology to make it easier for health providers to support better patient care and accelerate the path to full recovery. Doctors and physical therapists are given access to modern tooling needed to collect and track patient data at scale. From this data, they are able to make informed decisions about the health of their patients without risking losing the data to the wrong hands.


Furthermore, the collection of health data does not have to be boring or even happen in the clinic. A modular design allows developers to design their own applications and data streams allowing end-users to collect data on their own devices. We believe the process can be made fun and gamified because technology should be available to old and young. 

We've begun to develop the first stages of this technology and research potential applications in real-world patient settings. We have utilized electromyography and gyroscopic precession with gamification to improve physical therapy care. Check out our cool research below.

[Check out our Research](../../img/blogs/research.jpg)

# 3D Printing
The research space of prosthetics and 3D printing technology isn't new and by far complete. Technology can bridge the gap between immobility and mobility. Creative engineering and a commitment to use technology for social equity brought us to research this space further. What we found is that cheap materials are adequate for developing customized prosthetics for children with the proper equipment and software. The use of soft materials, however, requires expensive and hard-to-find equipment as the field is still quite new. Instead, existing equipment can be used with some modification. We are quite excited about the possibilities of composite and mixed material printing and continue to research further. Check out an initial prototype below.

[See a Prototype](../../img/blogs/handprototype.jpg)